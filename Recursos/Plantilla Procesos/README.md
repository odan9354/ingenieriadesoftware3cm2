1. EL documento principal es el main.tex donde verificar la estructura entera de la plantilla.
2.Empezaremos abriendo el archivo resumen.tex, en donde nos encargaremos de describir el proceso, añadiendo la imagen correspondiente del diagrama de procesos.
3.Posteriormente pasaremos a la sección de Elementos del proceso, en Elementos.tex vamos a llamar a nuestros elementos con \input{Elementos/elementoN.tex} donde dentro de cada elementoN
colocaremos \elemento recordando que el orden y lo que se va a llenar en la tabla viene en Elementos.tex en forma de comentario.
4.Por último la sección Tareas se ira llenando al igual que Elementos.tex  donde llamaremos a llamar a nuestra tareaN respectiva, la cual se llenará
con los comandos \nombreTarea que es el respectivo nombre de la tarea y \tarea en donde primero tenemos el nombre de la tarea y posteriormente 
la descripción de la tarea.
5.Repetiremos estos 4 pasos para la creación de cada proceso que se encuentre dentro del proyecto.

